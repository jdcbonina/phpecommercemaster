<?php 
	require "connection.php";

	$image = $_FILES['image'];
	$file_types = ["jpg", "jpeg", "png", "gif", "svg", "webp", "bitmap", "tiff", "tif"];
	$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

	function validateForm(){
		$image = $_FILES['image'];
		$file_types = ["jpg", "jpeg", "png", "gif", "svg", "webp", "bitmap", "tiff", "tif"];
		$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
		
		$errors = 0;	
		
		if(!in_array($file_ext, $file_types)){
			$errors++;
		}

		if($errors > 0){
			return false;
		}else{
			return true;
		}
	}

if(validateForm()){
	$destination = "../assets/images/";
	$fileName = $image['name'];

	$imgPath = $destination.$fileName;
	move_uploaded_file($image['tmp_name'], $imgPath);

	$add_item_query = "INSERT INTO users (imgPath) VALUES ($imgPath)";

	$new_item = mysqli_query($conn, $add_item_query);

	header("Location: ". $_SERVER['HTTP_REFERER']);
}else{
	header("localeconv()cation: ". $_SERVER['HTTP_REFERER']);
}

 ?>